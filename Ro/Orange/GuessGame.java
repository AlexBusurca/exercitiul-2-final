package Ro.Orange;

import java.util.Random;

public class GuessGame {
    static {System.out.println("Let's play guess the number.");}
    private Random number = new Random();
    private int guessThisNumber = number.nextInt(10);

    public void startGame() {

        Player A = new Player();
        Player B = new Player();
        Player C = new Player();
        System.out.println("I'm thinking about number: "+ guessThisNumber);

        int GA=A.getNumber();
        int GB=B.getNumber();
        int GC=C.getNumber();
        System.out.println("A: i'm guessing:" + GA);
        System.out.println("B: i'm guessing:" + GB);
        System.out.println("C: i'm guessing:" + GC);
        System.out.println("Player A guessed: " + GA);
        System.out.println("Player B guessed: " + GB);
        System.out.println("Player C guessed: " + GC);

        boolean allGuessesEqual;
        if (GA==GB&&GB==GC) {allGuessesEqual=true;
        }
         else {allGuessesEqual=false;}


        if ((GA == GB) || (GA == GC) || (GB == GC) || allGuessesEqual) {
            System.out.println("2 or all players used the same number. This guessing session is not valid and players will have to try again!");
            startGame();
        }
        else if ((guessThisNumber != GA) && (guessThisNumber != GB) && (guessThisNumber != GC)) {
            System.out.println("Players will have to try again!");
            startGame();
        }
        else if (guessThisNumber==GA) {
        System.out.println("We have a winner!");
        System.out.println("Player A got it right? true");
        System.out.println("Player B got it right? false");
        System.out.println("Player C got it right? false");
        System.out.println("This game is over.");
        }
        else if (guessThisNumber==GB) {
            System.out.println("We have a winner!");
            System.out.println("Player A got it right? false");
            System.out.println("Player B got it right? true");
            System.out.println("Player C got it right? false");
            System.out.println("This game is over.");
        }
        else if (guessThisNumber==GC) {
            System.out.println("We have a winner!");
            System.out.println("Player A got it right? false");
            System.out.println("Player B got it right? false");
            System.out.println("Player C got it right? true");
            System.out.println("This game is over.");
        }

        else {
        System.out.println("Nu trebuia sa iasa pe ramura asta!!!");
        }
    }
}


